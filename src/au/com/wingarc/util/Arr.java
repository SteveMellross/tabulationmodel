package au.com.wingarc.util;

import java.util.Collection;
import java.util.function.IntFunction;

public class Arr {

    @SafeVarargs
    public static <T> T[] of(T... items) {
        return items;
    }

    public static int[] ofInt(int... items) {
        return items;
    }

    public static double[] ofDouble(double... items) {
        return items;
    }

    public static <T> T[] ofCollection(Collection<T> collection, IntFunction<T[]> generator) {
        return collection.toArray(generator.apply(collection.size()));
    }
}
