package au.com.wingarc;

import au.com.wingarc.cube.Cube;
import au.com.wingarc.cube.CubeDefinition;
import au.com.wingarc.cube.TabulationCallbacks;
import au.com.wingarc.schema.field.ClassificationField;
import au.com.wingarc.schema.field.ForeignKeyField;
import au.com.wingarc.schema.field.NumericField;
import au.com.wingarc.schema.field.data.IntFieldData;
import au.com.wingarc.schema.summation.CountSummation;
import au.com.wingarc.schema.summation.MeasureSummation;
import au.com.wingarc.schema.table.ClassificationTable;
import au.com.wingarc.schema.table.FactTable;
import au.com.wingarc.util.Arr;

import java.text.DecimalFormat;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        ClassificationTable fourValues = new ClassificationTable(Arr.of("Val 1", "Val 2", "Val 3", "Val 4"));

        FactTable factTable2 = new FactTable("Fact Table 2");
        ClassificationField ft2Field1 = new ClassificationField("Classification Field 1", factTable2, fourValues, new IntFieldData("Int Field 5", Arr.ofInt(1, 0)));
        factTable2.finaliseFields();

        FactTable factTable1 = new FactTable("Fact Table 1");
        ClassificationField field1 = new ClassificationField("Classification Field 1", factTable1, fourValues, new IntFieldData("Int Field 1", Arr.ofInt(1, 1, 0, 0)));
        ClassificationField field2 = new ClassificationField("Classification Field 2", factTable1, fourValues, new IntFieldData("Int Field 2", Arr.ofInt(3, 3, 1, 0)));
        NumericField numericField1 = new NumericField("Numeric Field 1", factTable1, new IntFieldData("Int Field 3", Arr.ofInt(10, 100, 1000, 10000)));
        NumericField numericField2 = new NumericField("Numeric Field 2", factTable1, new IntFieldData("Int Field 4", Arr.ofInt(10, 10, 10, 10)));
        new ForeignKeyField(factTable1, new IntFieldData("Int Field 6", Arr.ofInt(1, 0, 1, 0)), factTable2);
        factTable1.finaliseFields();

        factTable1.finaliseSchema();
        factTable2.finaliseSchema();

        DecimalFormat df = new DecimalFormat("#.##");

        TabulationCallbacks callbacks = new TabulationCallbacks.Builder()
                .setOnRowStart((cubeDefinition, indexes) -> System.out.printf("ROW: %1$6d: [%2$s]\n", indexes[0], cubeDefinition.getNumericFieldStream().map(field -> df.format(field.getAsDouble(indexes[0]))).collect(Collectors.joining(", "))))
                .setOnCellUpdate((cubeDefinition, cube, pos, prevVal, add, currentValue) -> System.out.printf(" -CELL: %1$6d    | %2$6s + %3$6s = %4$6s\n", pos, df.format(prevVal), df.format(add), df.format(currentValue)))
                .setOnRowEnd((cubeDefinition, indexes) -> System.out.println())
                .build();

        CubeDefinition cubeDefinition = new CubeDefinition(
                Arr.of(new CountSummation(factTable1), new MeasureSummation(numericField1), new MeasureSummation(numericField2)),
                Arr.of(field2, field1),
                callbacks
        );

        Cube cube = cubeDefinition.tabulate();
    }
}
