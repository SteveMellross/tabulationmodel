package au.com.wingarc.cube;

public class Cube {
    private final CubeDefinition cubeDefinition;
    private final double[] values;

    public Cube(CubeDefinition cubeDefinition) {
        this.cubeDefinition = cubeDefinition;
        this.values = new double[Math.toIntExact(this.cubeDefinition.size())];
    }

    public double getValue(long pos) {
        return values[(int) pos];
    }

    public void addValue(long pos, double value) {
        values[(int) pos] += value;
    }

    public void addValue(long pos, double value, TabulationCallbacks.AddValueCallback addValueCallback) {
        double prevVal = getValue(pos);
        addValue(pos, value);
        addValueCallback.onUpdate(pos, prevVal, value, getValue(pos));
    }

    public void addValue(int[] indexes, double value) {
        addValue(cubeDefinition.toPos(indexes), value);
    }
}
