package au.com.wingarc.cube;

public class TabulationCallbacks {
    public static final TabulationCallbacks NONE = new TabulationCallbacks.Builder().build();

    public interface RowEvent {
        RowEvent NONE = (cubeDefinition, indexes) -> {};
        void accept(CubeDefinition cubeDefinition, int[] indexes);
    }

    public interface CellUpdate {
        CellUpdate NONE = (cubeDefinition, cube, pos, prevVal, add, currentValue) -> {};
        void accept(CubeDefinition cubeDefinition, Cube cube, long pos, double prevVal, double add, double currentValue);
    }

    interface AddValueCallback {
        void onUpdate(long pos, double prevValue, double add, double currentValue);
    }

    public static class Builder {
        private RowEvent onRowStart;
        private CellUpdate onCellUpdate;
        private RowEvent onRowEnd;

        public Builder setOnRowStart(RowEvent onRowStart) {
            this.onRowStart = onRowStart;
            return this;
        }

        public Builder setOnCellUpdate(CellUpdate onCellUpdate) {
            this.onCellUpdate = onCellUpdate;
            return this;
        }

        public Builder setOnRowEnd(RowEvent onRowEnd) {
            this.onRowEnd = onRowEnd;
            return this;
        }

        public TabulationCallbacks build() {
            return new TabulationCallbacks(onRowStart, onCellUpdate, onRowEnd);
        }
    }

    private final RowEvent onRowStart;
    private final CellUpdate onCellUpdate;
    private final RowEvent onRowEnd;

    public TabulationCallbacks(RowEvent onRowStart, CellUpdate onCellUpdate, RowEvent onRowEnd) {
        this.onRowStart = onRowStart == null ? RowEvent.NONE : onRowStart;
        this.onCellUpdate = onCellUpdate == null ? CellUpdate.NONE : onCellUpdate;
        this.onRowEnd = onRowEnd == null ? RowEvent.NONE : onRowEnd;
    }

    public void onRowStart(CubeDefinition cubeDefinition, int[] indexes) {
        onRowStart.accept(cubeDefinition, indexes);
    }

    public void onCellUpdate(CubeDefinition cubeDefinition, Cube cube, long pos, double prevVal, double add, double currentValue) {
        onCellUpdate.accept(cubeDefinition, cube, pos, prevVal, add, currentValue);
    }

    public void onRowEnd(CubeDefinition cubeDefinition, int[] indexes) {
        onRowEnd.accept(cubeDefinition, indexes);
    }

    public AddValueCallback getOnAddValue(CubeDefinition cubeDefinition, Cube cube) {
        return (pos, prevValue, add, currentValue) -> onCellUpdate(cubeDefinition, cube, pos, prevValue, add, currentValue);
    }
}
