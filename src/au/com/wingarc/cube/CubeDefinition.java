package au.com.wingarc.cube;

import au.com.wingarc.schema.field.ClassificationField;
import au.com.wingarc.schema.field.FactTableField;
import au.com.wingarc.schema.field.data.FieldData;
import au.com.wingarc.schema.summation.MeasureSummation;
import au.com.wingarc.schema.summation.Summation;

import java.util.Arrays;
import java.util.stream.Stream;

public class CubeDefinition {
    private final Summation[] summations;
    private final ClassificationField[] classifications;
    private final FieldData.Numeric[] numericFields;
    private final TabulationCallbacks tabulationCallbacks;
    private final long size;

    public CubeDefinition(Summation[] summations, ClassificationField[] classifications, TabulationCallbacks tabulationCallbacks) {
        this.summations = summations;
        this.classifications = classifications;

        Stream<FactTableField<?>> factTableFields = Stream.concat(
                Arrays.stream(summations).filter(MeasureSummation.class::isInstance).map(MeasureSummation.class::cast).map(MeasureSummation::getNumericField),
                Arrays.stream(classifications)
        );
        this.numericFields = factTableFields.map(FactTableField::getFieldData).toArray(FieldData.Numeric[]::new);

        this.tabulationCallbacks = tabulationCallbacks == null ? TabulationCallbacks.NONE : tabulationCallbacks;
        this.size = Arrays.stream(this.classifications).mapToLong(FactTableField::rows).reduce(1, (a, b) -> a * b) * summations.length;
    }

    public Stream<FieldData.Numeric> getNumericFieldStream() {
        return Arrays.stream(numericFields);
    }

    public long toPos(int[] indexes) {
        long pos = 0;
        int i = 0;
        for (; i < classifications.length; i++) {
            pos = pos * classifications[i].rows() + indexes[i];
        }
        pos *= summations.length + indexes[i];
        return pos;
    }

    private long toPosAssumingSingleFactTable(int row) {
        long pos = 0;
        for (ClassificationField classificationField : classifications) {
            pos = pos * classificationField.rows() + classificationField.get(row);
        }
        pos *= summations.length;
        return pos;
    }

    public long size() {
        return size;
    }

    public Cube tabulate() {
        Cube cube = new Cube(this);
        int[] indexes = new int[1];

        TabulationCallbacks.AddValueCallback addValueCallback = tabulationCallbacks.getOnAddValue(this, cube);

        int size = classifications[0].rows();
        for (int row = 0; row < size; row++) {
            indexes[0] = row;
            tabulationCallbacks.onRowStart(this, indexes);

            long pos = toPosAssumingSingleFactTable(row);
            for (Summation summation : summations) {
                cube.addValue(pos, summation.get(row), addValueCallback);
                pos++;
            }

            tabulationCallbacks.onRowEnd(this, indexes);
        }

        return cube;
    }
}
