package au.com.wingarc.schema.field;

import au.com.wingarc.schema.field.data.FieldData;
import au.com.wingarc.schema.table.FactTable;

public class NumericField extends FactTableField<FieldData.Numeric> {
    public NumericField(String name, FactTable factTable, FieldData.Numeric dataField) {
        super(name, factTable, dataField);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
