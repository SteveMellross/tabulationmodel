package au.com.wingarc.schema.field;

import au.com.wingarc.schema.field.data.IntFieldData;
import au.com.wingarc.schema.table.FactTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ForeignKeyField extends FactTableField<IntFieldData> {
    private static final int[] EMPTY = new int[0];

    private final PrimaryKeyField primaryKeyField;
    private final ReverseLookup reverseLookup;

    public ForeignKeyField(FactTable factTable, IntFieldData fieldData, FactTable foreignFactTable) {
        this(factTable, fieldData, foreignFactTable.getPrimaryKeyField());
    }

    public ForeignKeyField(FactTable factTable, IntFieldData fieldData, PrimaryKeyField primaryKeyField) {
        super("FK: " + primaryKeyField.getName(), factTable, fieldData);
        this.primaryKeyField = primaryKeyField;
        this.reverseLookup = new ReverseLookup();
        getFactTable().getFieldsBuilder().addField(this);
    }

    public PrimaryKeyField getPrimaryKeyField() {
        return primaryKeyField;
    }

    public int get(int index) {
        return getFieldData().get(index);
    }

    public int[] reverseGet(int reverseIndex) {
        return reverseLookup.childRows[reverseIndex];
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public class ReverseLookup {
        private final int[][] childRows;

        private ReverseLookup() {
            Map<Integer, ArrayList<Integer>> indexMap = new HashMap<>();

            IntFieldData dataField = getFieldData();
            int size = dataField.rows();
            for (int i = 0; i < size; i++) {
                indexMap.computeIfAbsent(dataField.get(i), k -> new ArrayList<>()).add(i);
            }

            childRows = new int[getPrimaryKeyField().rows()][];
            Arrays.fill(childRows, EMPTY);

            for (Map.Entry<Integer, ArrayList<Integer>> entry : indexMap.entrySet()) {
                childRows[entry.getKey()] = entry.getValue().stream().mapToInt(i -> i).toArray();
            }
        }
    }
}
