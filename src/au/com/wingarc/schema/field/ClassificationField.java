package au.com.wingarc.schema.field;

import au.com.wingarc.schema.field.data.IntFieldData;
import au.com.wingarc.schema.table.ClassificationTable;
import au.com.wingarc.schema.table.FactTable;

public class ClassificationField extends FactTableField<IntFieldData> {
    private final ClassificationTable classificationTable;

    public ClassificationField(String name, FactTable factTable, ClassificationTable classificationTable, IntFieldData fieldData) {
        super(name, factTable, fieldData);
        this.classificationTable = classificationTable;
    }

    public ClassificationTable getClassificationTable() {
        return classificationTable;
    }

    public int get(int index) {
        return getFieldData().get(index);
    }

    public String getLabel(int index) {
        return classificationTable.getLabels().get(index);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
