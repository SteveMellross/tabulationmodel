package au.com.wingarc.schema.field;

import au.com.wingarc.schema.field.data.FieldData;
import au.com.wingarc.schema.table.FactTable;

public abstract class FactTableField<T extends FieldData> implements FactTable.Child {
    private final String name;
    private final FactTable factTable;
    private final T dataField;

    public FactTableField(String name, FactTable factTable, T dataField) {
        this.name = name;
        this.factTable = factTable;
        this.dataField = dataField;
        getFactTable().getFieldsBuilder().addField(this);
    }

    public String getName() {
        return name;
    }

    @Override
    public FactTable getFactTable() {
        return factTable;
    }

    public T getFieldData() {
        return dataField;
    }

    @Override
    public int rows() {
        return factTable.rows();
    }

    public abstract void accept(Visitor visitor);

    public interface Visitor {
        void visit(PrimaryKeyField field);
        void visit(ClassificationField field);
        void visit(NumericField field);
        void visit(ForeignKeyField field);
    }
}
