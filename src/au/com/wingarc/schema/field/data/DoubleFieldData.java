package au.com.wingarc.schema.field.data;

public class DoubleFieldData extends BaseFieldData implements FieldData.Numeric {
    private final double[] items;

    public DoubleFieldData(String name, double[] items) {
        super(name);
        this.items = items;
    }

    public double get(int index) {
        return items[index];
    }

    @Override
    public double getAsDouble(int index) {
        return get(index);
    }

    @Override
    public int rows() {
        return items.length;
    }

    public interface View extends Numeric.View {

        double get();
    }
}
