package au.com.wingarc.schema.field.data;

public interface FieldData {

    String getName();

    int rows();

    interface Numeric extends FieldData {

        double getAsDouble(int index);

        interface View {

            double getAsDouble();
        }
    }
}
