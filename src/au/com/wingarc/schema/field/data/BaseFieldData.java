package au.com.wingarc.schema.field.data;

public abstract class BaseFieldData implements FieldData {
    private final String name;

    public BaseFieldData(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public abstract int rows();
}
