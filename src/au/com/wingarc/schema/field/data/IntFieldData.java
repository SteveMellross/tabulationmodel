package au.com.wingarc.schema.field.data;

public class IntFieldData extends BaseFieldData implements FieldData.Numeric {
    private final int[] items;

    public IntFieldData(String name, int[] items) {
        super(name);
        this.items = items;
    }

    public int get(int index) {
        return items[index];
    }

    @Override
    public double getAsDouble(int index) {
        return get(index);
    }

    @Override
    public int rows() {
        return items.length;
    }
}
