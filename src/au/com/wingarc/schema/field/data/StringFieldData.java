package au.com.wingarc.schema.field.data;

public class StringFieldData extends ObjectFieldData<String> {

    public StringFieldData(String name, String[] items) {
        super(name, items);
    }

    public interface View extends ObjectFieldData.View<String> {}
}
