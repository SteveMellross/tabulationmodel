package au.com.wingarc.schema.field.data;

public class ObjectFieldData<T> extends BaseFieldData {
    private final T[] items;

    public ObjectFieldData(String name, T[] items) {
        super(name);
        this.items = items;
    }

    public T get(int index) {
        return items[index];
    }

    @Override
    public int rows() {
        return items.length;
    }

    public interface View<T> {

        T get();
    }
}
