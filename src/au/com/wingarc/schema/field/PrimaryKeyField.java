package au.com.wingarc.schema.field;

import au.com.wingarc.schema.field.data.IntFieldData;
import au.com.wingarc.schema.table.FactTable;

public class PrimaryKeyField extends FactTableField<IntFieldData> {

    public PrimaryKeyField(FactTable factTable, IntFieldData fieldData) {
        super("PK: " + factTable.getName(), factTable, fieldData);
        getFactTable().getFieldsBuilder().addField(this);
    }

    public int get(int index) {
        return getFieldData().get(index);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
