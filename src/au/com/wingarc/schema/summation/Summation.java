package au.com.wingarc.schema.summation;

public interface Summation {

    String getName();

    double get(int index);
}
