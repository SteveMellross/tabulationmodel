package au.com.wingarc.schema.summation;

import au.com.wingarc.schema.table.FactTable;

public class CountSummation implements Summation {
    private final FactTable factTable;

    public CountSummation(FactTable factTable) {
        this.factTable = factTable;
    }

    @Override
    public String getName() {
        return "Count of " + factTable.getName();
    }

    @Override
    public double get(int index) {
        return 1;
    }
}
