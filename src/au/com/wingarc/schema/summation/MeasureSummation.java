package au.com.wingarc.schema.summation;

import au.com.wingarc.schema.field.NumericField;

public class MeasureSummation implements Summation {
    private final NumericField numericField;

    public MeasureSummation(NumericField numericField) {
        this.numericField = numericField;
    }

    @Override
    public String getName() {
        return "Sum of " + numericField.getName();
    }

    public NumericField getNumericField() {
        return numericField;
    }

    @Override
    public double get(int index) {
        return getNumericField().getFieldData().getAsDouble(index);
    }
}
