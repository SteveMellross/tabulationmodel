package au.com.wingarc.schema.table;

import au.com.wingarc.schema.field.data.StringFieldData;

public class ClassificationTable {
    private final StringFieldData labels;

    public ClassificationTable(String[] labels) {
        this.labels = new StringFieldData("Values", labels);
    }

    public StringFieldData getLabels() {
        return labels;
    }
}
