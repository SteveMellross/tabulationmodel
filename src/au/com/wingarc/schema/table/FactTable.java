package au.com.wingarc.schema.table;

import au.com.wingarc.schema.field.ClassificationField;
import au.com.wingarc.schema.field.FactTableField;
import au.com.wingarc.schema.field.ForeignKeyField;
import au.com.wingarc.schema.field.NumericField;
import au.com.wingarc.schema.field.PrimaryKeyField;
import au.com.wingarc.schema.field.data.IntFieldData;
import au.com.wingarc.util.Arr;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class FactTable {

    private final String name;

    private PrimaryKeyField primaryKeyField;
    private ClassificationField[] classificationFields;
    private NumericField[] numericFields;
    private ForeignKeyField[] foreignKeyFields;

    private ForeignKeyField[] childForeignKeyFields;

    private int rows;

    private FieldsBuilder fieldsBuilder = new FieldsBuilder();  // Building while non-null
    private SchemaBuilder schemaBuilder = new SchemaBuilder();  // Building while non-null

    public FactTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int rows() {
        return rows;
    }

    public PrimaryKeyField getPrimaryKeyField() {
        return primaryKeyField;
    }

    public FieldsBuilder getFieldsBuilder() {
        if (fieldsBuilder == null) {
            throw new IllegalStateException("Fields already built");
        }
        return fieldsBuilder;
    }

    public void finaliseFields() {
        getFieldsBuilder().build();
    }

    public SchemaBuilder getSchemaBuilder() {
        if (schemaBuilder == null) {
            throw new IllegalStateException("Schema already built");
        }
        return schemaBuilder;
    }

    public void finaliseSchema() {
        getSchemaBuilder().build();
    }

    public interface Child {
        FactTable getFactTable();
        int rows();
    }

    public class FieldsBuilder {
        private final ArrayList<FactTableField<?>> unconstructedFields = new ArrayList<>();
        private int builderRows = -1;

        private void ensureBuilding() {
            if (fieldsBuilder == null) {
                throw new IllegalStateException("Already built");
            }
        }

        private <T extends FactTableField<?>> T verify(T field) {
            int fieldSize = field.getFieldData().rows();
            if (builderRows < 0) {
                builderRows = fieldSize;
            } else if (fieldSize != builderRows) {
                throw new IllegalStateException("Field sizes do not match");
            }
            return field;
        }

        public void addField(FactTableField<?> field) {
            ensureBuilding();
            unconstructedFields.add(field);
        }

        private void build() {
            ensureBuilding();

            ArrayList<ClassificationField> builderClassificationFields = new ArrayList<>();
            ArrayList<NumericField> builderNumericFields = new ArrayList<>();
            ArrayList<ForeignKeyField> builderForeignKeyFields = new ArrayList<>();

            FactTableField.Visitor fieldVisitor = new FactTableField.Visitor() {
                @Override
                public void visit(PrimaryKeyField field) {
                    throw new IllegalStateException("PK is automatically generated");
                }

                @Override
                public void visit(ClassificationField field) {
                    builderClassificationFields.add(verify(field));
                }

                @Override
                public void visit(NumericField field) {
                    builderNumericFields.add(verify(field));
                }

                @Override
                public void visit(ForeignKeyField field) {
                    builderForeignKeyFields.add(verify(field));
                    field.getPrimaryKeyField().getFactTable().getSchemaBuilder().addChildForeignKey(field);
                }
            };

            unconstructedFields.forEach(field -> field.accept(fieldVisitor));  // All constructed now.

            primaryKeyField = new PrimaryKeyField(FactTable.this, new IntFieldData(FactTable.this.getName(), IntStream.range(0, builderRows).toArray()));
            classificationFields = Arr.ofCollection(builderClassificationFields, ClassificationField[]::new);
            numericFields = Arr.ofCollection(builderNumericFields, NumericField[]::new);
            foreignKeyFields = Arr.ofCollection(builderForeignKeyFields, ForeignKeyField[]::new);

            rows = builderRows;
            fieldsBuilder = null;
        }
    }

    public class SchemaBuilder {
        private final ArrayList<ForeignKeyField> builderChildForeignKeyFields = new ArrayList<>();

        private void ensureBuilding() {
            if (schemaBuilder == null) {
                throw new IllegalStateException("Already built");
            }
        }

        private void addChildForeignKey(ForeignKeyField field) {
            ensureBuilding();
            builderChildForeignKeyFields.add(field);
        }

        private void build() {
            ensureBuilding();
            childForeignKeyFields = Arr.ofCollection(builderChildForeignKeyFields, ForeignKeyField[]::new);
        }
    }
}
